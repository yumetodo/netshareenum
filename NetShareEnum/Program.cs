﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace NetShareEnum
{
	/*
	typedef struct _SHARE_INFO_2
	{
		LPWSTR shi2_netname;
		DWORD shi2_type;
		LPWSTR shi2_remark;
		DWORD shi2_permissions;
		DWORD shi2_max_uses;
		DWORD shi2_current_uses;
		LPWSTR shi2_path;
		LPWSTR shi2_passwd;
	} SHARE_INFO_2, * PSHARE_INFO_2, * LPSHARE_INFO_2;
	*/
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct SHARE_INFO_2
	{
		[MarshalAs(UnmanagedType.LPWStr)]
		public string shi2_netname;
		public uint shi2_type;
		[MarshalAs(UnmanagedType.LPWStr)]
		public string shi2_remark;
		public uint shi2_permissions;
		public uint shi2_max_uses;
		public uint shi2_current_uses;
		[MarshalAs(UnmanagedType.LPWStr)]
		public string shi2_path;
		[MarshalAs(UnmanagedType.LPWStr)]
		public string shi2_passwd;
	}
    public enum NET_API_STATUS : uint
    {
        NERR_Success = 0,
        /// <summary>
        /// This computer name is invalid.
        /// </summary>
        NERR_InvalidComputer = 2351,
        /// <summary>
        /// This operation is only allowed on the primary domain controller of the domain.
        /// </summary>
        NERR_NotPrimary = 2226,
        /// <summary>
        /// This operation is not allowed on this special group.
        /// </summary>
        NERR_SpeGroupOp = 2234,
        /// <summary>
        /// This operation is not allowed on the last administrative account.
        /// </summary>
        NERR_LastAdmin = 2452,
        /// <summary>
        /// The password parameter is invalid.
        /// </summary>
        NERR_BadPassword = 2203,
        /// <summary>
        /// The password does not meet the password policy requirements.
        /// Check the minimum password length, password complexity and password history requirements.
        /// </summary>
        NERR_PasswordTooShort = 2245,
        /// <summary>
        /// The user name could not be found.
        /// </summary>
        NERR_UserNotFound = 2221,
        ERROR_ACCESS_DENIED = 5,
        ERROR_NOT_ENOUGH_MEMORY = 8,
        ERROR_INVALID_PARAMETER = 87,
        ERROR_INVALID_NAME = 123,
        ERROR_INVALID_LEVEL = 124,
        ERROR_MORE_DATA = 234 ,
        ERROR_SESSION_CREDENTIAL_CONFLICT = 1219,
        /// <summary>
        /// The RPC server is not available. This error is returned if a remote computer was specified in
        /// the lpServer parameter and the RPC server is not available.
        /// </summary>
        RPC_S_SERVER_UNAVAILABLE = 2147944122, // 0x800706BA
        /// <summary>
        /// Remote calls are not allowed for this process. This error is returned if a remote computer was
        /// specified in the lpServer parameter and remote calls are not allowed for this process.
        /// </summary>
        RPC_E_REMOTE_DISABLED = 2147549468 // 0x8001011C
    }
	class Program
	{
		/*
		 * NET_API_STATUS NetShareEnum(
		  LPWSTR servername,     // 実行対象のリモートサーバー
		  DWORD level,           // 情報レベル
		  LPBYTE *bufptr,        // 情報が格納されるバッファ
		  DWORD prefmaxlen,      // バッファの最大サイズ
		  LPDWORD entriesread,   // 格納されたエントリの数
		  LPDWORD totalentries,  // 利用可能なエントリの総数
		  LPDWORD resume_handle // レジュームハンドル
		);
		 */
		[DllImport("Netapi32.dll", SetLastError = true)]
		public static extern int NetShareEnum(
			[MarshalAs(UnmanagedType.LPWStr)] string serverName,// 実行対象のリモートサーバー
			Int32 level,// 情報レベル
			out IntPtr bufPtr,// 情報が格納されるバッファ
			Int32 prefmaxlen,// バッファの最大サイズ
			ref Int32 entriesread,// 格納されたエントリの数
			ref Int32 totalentries,// 格納されたエントリの数
			ref Int32 resume_handle// レジュームハンドル
		);
		[DllImport("Netapi32.dll", SetLastError = true)]
		public static extern long NetApiBufferFree(IntPtr bufPtr);
		public static List<SHARE_INFO_2> NetShareEnum_2_wrap(string serverName) {
			var re = new List <SHARE_INFO_2> { };
			Int32 er = 0;
			Int32 tr = 0;
			Int32 resume = 0;
			int res;
			do
			{
				res = NetShareEnum(serverName, 2, out var bufPtr, -1, ref er, ref tr, ref resume);
				if (res != (int)NET_API_STATUS.NERR_Success && res != (int)NET_API_STATUS.ERROR_MORE_DATA) break;
				var infoP = bufPtr;
				for (int i = 0; i < er; ++i)
				{
					re.Add(Marshal.PtrToStructure<SHARE_INFO_2>(infoP));
					infoP = IntPtr.Add(infoP, Marshal.SizeOf<SHARE_INFO_2>());
				}
				NetApiBufferFree(bufPtr);
			} while (res == (int)NET_API_STATUS.ERROR_MORE_DATA);
			return re;
		}
		static void Main(string[] args)
		{
			foreach(var r in NetShareEnum_2_wrap(""))
			{
				Console.WriteLine(r.shi2_netname);
			}
		}
	}
}
